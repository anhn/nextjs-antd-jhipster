const JHIPSTER_PROJECT_DIR = '.'

let ejs = require('ejs')
let fs = require('fs')

const appConfig = require(JHIPSTER_PROJECT_DIR + '/.yo-rc.json')
const { entities } = appConfig

const entityConfig = {}
entities.forEach(ent => {
  entityConfig[ent] = require(JHIPSTER_PROJECT_DIR + '/.jhipster/' + ent + '.json')
})

const toUpperCamelCase = str => str.replace(/\b\S/g, t => t.toUpperCase())
const toLowerCamelCase = str => str.replace(/\b\S/g, t => t.toLowerCase())
const toHyphenCase = str => str.replace(/[A-Z]/g, m => "-" + m.toLowerCase());

function generateEntity(name) {
  const conf = entityConfig[name]
  const entityName = toUpperCamelCase(name)
  const instanceName = toLowerCamelCase(name)
  const fileName = toHyphenCase(toLowerCamelCase(name))

  console.log(name)

  const ejsCfg = {
    appName: appConfig.baseName,
    entityName,
    instanceName,
    fileName,
    fields: conf.fields,
    relationships: conf.relationships
  }

  fs.mkdir('./_generated', () => {})
  fs.mkdir('./_generated/entities', () => {})
  fs.mkdir(`./_generated/entities/${fileName}`, () => {})
  ejsRender('./tpl/entity/entity.ejs', `./_generated/entities/${fileName}/entity.ts`, ejsCfg)
  ejsRender('./tpl/entity/edit-form.ejs', `./_generated/entities/${fileName}/edit-form.tsx`, ejsCfg)
  ejsRender('./tpl/entity/table.ejs', `./_generated/entities/${fileName}/table.tsx`, ejsCfg)
  ejsRender('./tpl/entity/collection.ejs', `./_generated/entities/${fileName}/collection.tsx`, ejsCfg)
}

function ejsRender(tplFile, targetFile, cfg) {
  ejs.renderFile(tplFile, cfg, {}, (err, str) => {
    if (err) {
      console.log(err)
      return
    }
    fs.writeFile(targetFile, str, (err, data) => {
      if (err) {
        console.log(err)
      }
    })
  })
}

entities.forEach(name => generateEntity(name))