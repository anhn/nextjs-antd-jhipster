module.exports = {
  endOfLine: 'lf',
  semi: false,
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 110,
  tabWidth: 2,
};
