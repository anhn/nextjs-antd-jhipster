/// <reference types="next" />
/// <reference types="next/types/global" />

declare module "*.less";
declare module "*.css";
declare module "*.svg";
declare module "*.png";
declare module "*.jpg";
declare module "*.jpeg";

declare module "*.module.less" {
    const classes: { [key: string]: string };
    export default classes;
}