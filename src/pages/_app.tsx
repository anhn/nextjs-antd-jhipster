import 'mobx-react-lite/batchingForReactDom'
import React from 'react'
import { AppProps } from 'next/app'

//Global antd styles
import 'antd/dist/antd.less'

import { createRootStore, hydrateRootStore } from 'model/root-store/setup-root-store'
import { RootStoreProvider } from 'model/root-store/root-store-context'

function MyApp({ Component, pageProps /*, router*/ }: AppProps) {
  const [rootStore] = React.useState(createRootStore())
  React.useMemo(() => hydrateRootStore(rootStore), [])

  return <RootStoreProvider value={rootStore}>
    <Component {...pageProps} />
  </RootStoreProvider>
}

export default MyApp
