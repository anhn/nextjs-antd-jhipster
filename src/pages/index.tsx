import { Layout } from 'antd'
import { AppHeader } from 'components/header'

const { Content } = Layout

const Page = () => {
  return <Layout>
    <AppHeader />
    <Content style={{ padding: 24 }}>
    </Content>
  </Layout>
}

export default Page