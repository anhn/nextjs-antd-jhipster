import React from 'react'
import { Layout, PageHeader } from 'antd'
import { AppHeader } from 'components/header'
import { PurchaseOrderEdit } from 'functional/purchase-order-edit'

const Page = () => {
  return <Layout>
    <AppHeader />
    <PageHeader title="Purchase Order" />
    <Layout.Content style={{ padding: 16 }}>
      <PurchaseOrderEdit />
    </Layout.Content>
  </Layout>
}

export default Page