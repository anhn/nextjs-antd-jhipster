import React from 'react'
import { Layout, PageHeader, message } from 'antd'
import { AppHeader } from 'components/header'
import { PurchaseOrderCollection } from 'components/purchase-order/collection'
import { useRootStore } from 'model/root-store/root-store-context'
import { usePurchaseOrders } from 'services/hooks/swrHooks'
import { observer } from 'mobx-react-lite'
import { globalApi } from 'services/api'

const { Content } = Layout

const Page = observer(() => {
  const { hydrated } = useRootStore()
  const entities = usePurchaseOrders(hydrated)

  const onCreateProduct = async (product) => {
    const { ok, data, msg } = await globalApi.entities.purchaseOrder.create(product)
    if (ok) {
      message.success('Thành công')
    } else {
      message.error(msg)
    }
  }
  const onUpdateProduct = async (product) => {
    const { ok, data, msg } = await globalApi.entities.purchaseOrder.update(product)
    if (ok) {
      message.success('Thành công')
    } else {
      message.error(msg)
    }
  }
  const onRemoveProduct = async (product) => {
    const { ok, data, msg } = await globalApi.entities.purchaseOrder.remove(product.id)
    if (ok) {
      message.success('Thành công')
    } else {
      message.error(msg)
    }
  }
  return <Layout>
    <AppHeader />
    <PageHeader title="Purchase Order" />
    <Content style={{ padding: 16 }}>
      <PurchaseOrderCollection
        value={entities}
        onCreateItem={onCreateProduct}
        onUpdateItem={onUpdateProduct}
        onRemoveItem={onRemoveProduct}
      />
    </Content>
  </Layout>
})

export default Page