import React from 'react'
import { Layout, PageHeader, message } from 'antd'
import { AppHeader } from 'components/header'
import { ProductCollection } from 'components/product/collection'
import { useRootStore } from 'model/root-store/root-store-context'
import { useProducts } from 'services/hooks/swrHooks'
import { observer } from 'mobx-react-lite'
import { globalApi } from 'services/api'

const { Content } = Layout

const Page = observer(() => {
    const { hydrated } = useRootStore()
    const products = useProducts(hydrated)
    const onCreateProduct = async (product) => {
        const { ok, data, msg } = await globalApi.entities.product.create(product)
        if (ok) {
            message.success('Thành công')
        } else {
            message.error(msg)
        }
    }
    const onUpdateProduct = async (product) => {
        const { ok, data, msg } = await globalApi.entities.product.update(product)
        if (ok) {
            message.success('Thành công')
        } else {
            message.error(msg)
        }
    }
    const onRemoveProduct = async (product) => {
        const { ok, data, msg } = await globalApi.entities.product.remove(product.id)
        if (ok) {
            message.success('Thành công')
        } else {
            message.error(msg)
        }
    }
    return <Layout>
        <AppHeader />
        <PageHeader title="Product" />
        <Content style={{ padding: 16 }}>
            <ProductCollection
                value={products}
                onCreateItem={onCreateProduct}
                onUpdateItem={onUpdateProduct}
                onRemoveItem={onRemoveProduct}
            />
        </Content>
    </Layout>
})

export default Page