import { Instance, SnapshotOut, types, tryResolve } from 'mobx-state-tree'
import { maybeUndefinedNull } from 'model/common'
import { AuthModel } from 'model/auth-model'
import { AppStateModel } from 'model/app-state-model'

/**
 * A RootStore model.
 */
export const RootStoreModel = types
  .model('RootStore')
  .props({
    /// Indicaton of finish initializing root store
    hydrated: maybeUndefinedNull(types.boolean),
    /// Common app states (UI related, etc.)
    app: types.optional(AppStateModel, {}),
    /// Authentication info
    auth: types.optional(AuthModel, {})
  })
  .actions(self => ({
    onHydrationComplete: () => {
      console.log('RootStore hydration completed')
      self.hydrated = true
    },
  }))

export type RootStore = Instance<typeof RootStoreModel>
export type RootStoreSnapshot = SnapshotOut<typeof RootStoreModel>
