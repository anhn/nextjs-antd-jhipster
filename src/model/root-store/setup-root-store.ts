import defaultStorage from './storage'
import { RootStore, RootStoreModel } from './root-store-model'
import { onSnapshot, onPatch, getSnapshot, applySnapshot } from 'mobx-state-tree'
import { globalApi } from 'services/api'


/**
 * The key we'll be saving our state as within async storage.
 */
const ROOT_STATE_STORAGE_KEY = 'jhRootState'
const storage = defaultStorage

export const createRootStore = () => {
  const rootStore = RootStoreModel.create({})
  // storage.save(ROOT_STATE_STORAGE_KEY, getSnapshot(rootStore))
  // track changes & save to storage
  onSnapshot(rootStore, snapshot => {
    console.log('onSnapshot')
    storage.save(ROOT_STATE_STORAGE_KEY, snapshot)
  })

  onPatch(rootStore, patch => {
    console.log(patch)
    const { op, path, value } = patch
    if (op === 'replace' && path === '/auth/token') {
      if (value) {
        // nookies.set({}, 'tintoken', value, { path: '/', maxAge: 12 * 60 * 60 })
        // rootStore.auth.validateAuthentication()
        if (value !== globalApi.jhipster.accessToken) {
          console.log('UpdateApiToken', value)
          globalApi.jhipster.setToken(value)
        }
      }
    }
  })
  return rootStore
}

export const hydrateRootStore = async (rootStore: RootStore) => {
  try {
    // load data
    const data = (await storage.load(ROOT_STATE_STORAGE_KEY))
    if (data) {
      applySnapshot(rootStore, data)
    }
    rootStore.onHydrationComplete()
  } catch (e) {
    console.log('Hydration failed')
  }
}