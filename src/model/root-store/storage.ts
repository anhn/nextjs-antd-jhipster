/**
 * Simple im-memory storage compatible with localStorage/sessionStorage API.
 */
export class InMemoryStorage implements Storage {
  private items = {} as { [key: string]: string | any }

  get length(): number {
    return Object.keys(this.items).length
  }

  public clear(): void {
    this.items = {}
  }

  public getItem(key: string): string | any {
    return this.items[key]
  }

  /**
   * @deprecated operation not supported
   */
  public key(index: number): string | any {
    throw new Error('Unsupported operation')
  }

  public removeItem(key: string): void {
    delete this.items[key]
  }

  public setItem(key: string, data: string): void {
    this.items[key] = data
  }

  public async load(key: string): Promise<any | null> {
    try {
      return JSON.parse(this.items[key])
    } catch {
      return null
    }
  }

  public async save(key: string, value: any): Promise<boolean> {
    try {
      this.items[key] = value
      return true
    } catch {
      return false
    }
  }

  [key: string]: any
  [index: number]: string
}

let defaultStorage: Storage = new InMemoryStorage()
if (typeof localStorage !== 'undefined') {
  defaultStorage = localStorage
}
// else if (typeof AsyncStorage !== 'undefined') {
//   defaultStorage = new AsyncStorage() as any
// }

class LocalStorage {
  get length(): number {
    return defaultStorage.length
  }

  public clear(): void {
    defaultStorage.clear()
  }

  public getItem(key: string): string | any {
    return defaultStorage.getItem(key)
  }

  /**
   * @deprecated operation not supported
   */
  public key(index: number): string | any {
    return defaultStorage.key(index)
  }

  public removeItem(key: string): void {
    defaultStorage.removeItem(key)
  }

  public setItem(key: string, data: string): void {
    defaultStorage.setItem(key, data)
  }

  public async load(key: string): Promise<any | null> {
    try {
      return JSON.parse(this.getItem(key))
    } catch {
      return null
    }
  }

  public async save(key: string, value: any): Promise<boolean> {
    try {
      this.setItem(key, JSON.stringify(value))
      return true
    } catch {
      return false
    }
  }
}

export default new LocalStorage()
