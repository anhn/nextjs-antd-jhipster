import { IAnyType, IMaybeIType, IStateTreeNode, types } from 'mobx-state-tree'

interface IMaybeUndefinedNull<IT extends IAnyType> extends IMaybeIType<IT, null | undefined, undefined> { }

export function maybeUndefinedNull<T extends IAnyType>(model: T): IMaybeUndefinedNull<T> {
  return types.union(model, types.optional(types.undefined, undefined), types.optional(types.null, null))
}


/**
 * Merge field updates into current node
 * @param self
 */
export const extMerger = (self: IStateTreeNode) => ({
  actions: {
    merge(updates: any) {
      Object.keys(updates).forEach(name => {
        self[name] = updates[name]
      })
    },
    update: (fieldName: string, value: any) => {
      self[fieldName] = value
    },
    handleChange: (fieldName: string, value: any) => {
      self[fieldName] = value
    },
  },
})
