import { types, flow } from 'mobx-state-tree'
import { UserModel } from './user-model'
import { globalApi } from 'services/api'

const api = globalApi.jhipster

export const AuthModel = types.model('Auth', {
  token: types.maybe(types.string),
  user: types.maybe(UserModel),
})
  .actions(self => ({
    loginPassword: flow(function* (username: string, password: string) {
      const response = yield api.loginPassword(username, password)
      const { ok, data } = response
      if (ok) {
        const { id_token } = data
        self.token = id_token
      }
      return response
    }),
    logout: () => {
      self.token = undefined
    },
    getUserInfo: flow(function* () {
      const response = yield api.getAccount()
      const { ok, data } = response
      if (ok) {
        self.user = UserModel.create(data)
      }
      return response
    })
  }))
  .views(self => ({
    get isAuthenticated() {
      if (self.token) {
        return true
      }
      return false
    }
  }))
