import { types, flow } from 'mobx-state-tree'
import { extMerger } from './common'

/**
 * Some global app states, mostly UI related
 */
export const AppStateModel = types.model('AppState')
  .props({
    loginModalVisible: false,
  })
  .actions(self => ({
    showLoginModal: (visible: boolean = false) => {
      self.loginModalVisible = visible
    },
    toggleLoginModal: () => {
      self.loginModalVisible = !self.loginModalVisible
    },
  }))
  .actions(self => ({
    requireAuthentication: flow(function* (asyncCall?: any) {
      self.showLoginModal(true)
    })
  }))
  .extend(extMerger)