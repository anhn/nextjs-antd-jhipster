import { Instance, SnapshotOut, types } from 'mobx-state-tree'
import { maybeUndefinedNull } from './common'

export const UserModel = types.model('User')
  .props({
    id: types.identifierNumber,
    login: maybeUndefinedNull(types.string),
    firstName: maybeUndefinedNull(types.string),
    lastName: maybeUndefinedNull(types.string),
    email: maybeUndefinedNull(types.string),
    imageUrl: maybeUndefinedNull(types.string),
    activated: maybeUndefinedNull(types.string),
    langKey: maybeUndefinedNull(types.string),
    authorities: maybeUndefinedNull(types.array(types.string))
  })

export type UserType = Instance<typeof UserModel>
export type UserSnapshot = SnapshotOut<typeof UserModel>

export interface IUserSnapshot extends UserSnapshot { }
export interface IUserType extends UserType { }

export type IUser = Partial<IUserType>