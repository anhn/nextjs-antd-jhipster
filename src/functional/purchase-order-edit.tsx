import React from 'react'
import { useRouter } from 'next/dist/client/router'
import { useRootStore } from 'model/root-store/root-store-context'
import { useProducts, usePurchaseOrder } from 'services/hooks/swrHooks'
import { Select, Form, message } from 'antd'
import { PurchaseOrderFormView } from 'components/purchase-order/edit-form'
import { observer } from 'mobx-react-lite'
import { globalApi } from 'services/api'

interface Props {
    entity?: any
}

export const PurchaseOrderEdit = observer((props: Props) => {
    const { hydrated } = useRootStore()
    const router = useRouter()
    const id = router.query.id as string

    const purchaseOrder = usePurchaseOrder(hydrated && !!id, id)
    const allProducts = useProducts(hydrated)
    const productSelectOptions = React.useMemo(() => {
        return allProducts.map(p => <Select.Option key={p.id} value={p.id}>{p.name}</Select.Option>)
    }, [allProducts])

    const [form] = Form.useForm()
    const formFields = ['id', 'code', 'total', 'productLines']

    React.useEffect(() => {
        if (purchaseOrder) {
            form.setFieldsValue(purchaseOrder)
        }
    }, [purchaseOrder])

    const onSubmit = async (values) => {
        console.log('SUBMIT', values)
        const { ok, data, msg } = await globalApi.entities.purchaseOrder.put(values)

        if (ok) {
            message.success('Lưu thành công')
            form.setFieldsValue(data);
        } else {
            message.error(`Lưu thất bại: ${msg}`)
        }
    }
    const calculateOrder = async () => {
        const values = form.getFieldsValue(formFields)
        // Keep deleted lines
        const deletedLines = values.productLines.filter(line => line.markDelete)
        console.log('Calculating', values)
        const { ok, data, msg } = await globalApi.entities.purchaseOrder.calculate(values)

        if (ok) {
            message.success('Thành công')
            // Add deleted lines back
            console.log('Add back', deletedLines)
            data.productLines.push(...deletedLines)
            form.setFieldsValue(data);
        } else {
            message.error(msg)
        }
    }

    const onFieldsChange = (changedFields: any[]) => {
        console.log('FIELD_CHANGED', changedFields)
        if (changedFields.length === 0) {
            return
        }
        for (const field of changedFields) {
            console.log('CHECKING', field.name)
            // recalculate if product lines changed
            if (field.name[0] === 'productLines') {
                calculateOrder()
            }
        }
    }

    return <PurchaseOrderFormView
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 12 }}
        form={form}
        onSubmit={onSubmit}
        onFieldsChange={onFieldsChange}
        productSelectOptions={productSelectOptions}
        {...props}
    />
})