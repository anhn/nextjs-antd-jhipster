import React from 'react'
import useSWR from 'swr'
import { createSWRFetcher, globalApi } from 'services/api'

export const useProducts = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'product/getAll' : null, createSWRFetcher(globalApi.entities.product.getAll, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}
export const useProduct = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'product/get' : null, createSWRFetcher(globalApi.entities.product.get, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}

export const usePurchaseOrders = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'purchase-order/getAll' : null, createSWRFetcher(globalApi.entities.purchaseOrder.getAll, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}

export const usePurchaseOrder = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'purchase-order/get' : null, createSWRFetcher(globalApi.entities.purchaseOrder.get, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}

export const useOrderProductLines = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'order-product-line/getAll' : null, createSWRFetcher(globalApi.entities.orderProductLine.getAll, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}

export const useOrderProductLine = (hydrated: boolean, ...args) => {
    const { data, error } = useSWR(hydrated ? 'order-product-line/get' : null, createSWRFetcher(globalApi.entities.orderProductLine.get, ...args), { revalidateOnFocus: false })
    if (error || !data) {
        return []
    }
    return data
}