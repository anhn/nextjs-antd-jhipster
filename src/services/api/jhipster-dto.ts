export class TokenResponse {
  id_token?: string
}
export interface AccountDTO {
  id?: number
  login?: string
  firstName?: string
  lastName?: string
  email?: string
  imageUrl?: string
  activated?: string
  langKey?: string
  authorities?: string[]
  createdBy?: string
  createdDate?: any
  lastModifiedBy?: string
  lastModifiedDate?: any
}