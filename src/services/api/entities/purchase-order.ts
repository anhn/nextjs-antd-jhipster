import { createEntityApi, JHipsterApi } from "../jhipster-api"

export const createPurchaseOrderApi = (jApi: JHipsterApi) => ({
    calculate: param => jApi.calculatePurchaseOrder(param),
    ...createEntityApi(jApi, 'api/purchase-orders'),
})