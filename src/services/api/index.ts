import { JHipsterApi, createEntityApi } from './jhipster-api'
import { createPurchaseOrderApi } from './entities/purchase-order'

const jApi = new JHipsterApi()

export const createSWRFetcher = (apiCall, ...args) => {
  return () => apiCall(...args)
    .then(result => {
      const { ok, data, text } = result
      if (!ok) {
        throw new Error(text)
      }
      return result.data
    })
}

export const globalApi = {
  jhipster: jApi,
  entities: {
    product: createEntityApi(jApi, 'api/products'),
    purchaseOrder: createPurchaseOrderApi(jApi),
    orderProductLine: createEntityApi(jApi, 'api/order-product-lines'),
  }
}

export type GlobalApi = typeof globalApi
