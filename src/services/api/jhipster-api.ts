import { create, ApiResponse, ApiErrorResponse } from 'apisauce'
import { TokenResponse, AccountDTO } from './jhipster-dto'
import qs from 'qs'
import { IPurchaseOrder } from 'components/purchase-order/entity'

export type ApiResult<T = any, U = any> = OkResult<T> | ErrorResult<U>

export interface OkResult<T> {
  ok: true
  msg?: string
  data?: T
}

export interface ErrorResult<U> {
  ok: false
  code?: number
  msg?: string
  data?: U
  temporary?: boolean
}

export class JHipsterApi {
  public apiBaseUrl: string = process.env.NEXT_PUBLIC_BASE_URL || 'http://localhost:8080/'
  sauce = create({ baseURL: this.apiBaseUrl, timeout: 15000 })
  anonSauce = create({ baseURL: this.apiBaseUrl, timeout: 15000 })
  _token: string

  constructor(url: string = process.env.NEXT_PUBLIC_BASE_URL || 'http://localhost:8080') {
    this.apiBaseUrl = url
    this.sauce = create({ baseURL: this.apiBaseUrl, timeout: 15000 })
    this.anonSauce = create({ baseURL: this.apiBaseUrl, timeout: 15000 })
  }

  public get accessToken() {
    return this._token
  }
  /**
   * Assign new access_token and set request Authorization header
   */
  public set accessToken(token: string) {
    this._token = token
    if (!token || token.length) {
      this.sauce.setHeader('Authorization', `Bearer ${token}`)
    } else {
      this.sauce.deleteHeader('Authorization')
    }
  }
  /**
   * Set auth token
   * @param token
   */
  public setToken(token?: string) {
    console.log('Setting api token', token)
    this.accessToken = token
  }

  /**
   * Default jhipster login with username and password
   * @param username 
   * @param password 
   */
  public async loginPassword(username: string, password: string): Promise<ApiResult<TokenResponse>> {
    const response = await this.sauce.post<TokenResponse, any>('api/authenticate', { username, password })
    const { ok, data } = response
    if (ok) {
      const { id_token } = data as any
      this.setToken(id_token)
      return { ok, data }
    } else {
      return this._getErrorResult(response)
    }
  }
  /**
   * jhipster get current authenticated account info including roles
   */
  public async getAccount(): Promise<ApiResult<AccountDTO>> {
    const apiCall = () => this.sauce.get<AccountDTO>('api/account')
    const response = await apiCall()
    return this._getErrorResult(response)
  }

  public async entityGet<T>(endpoint: string, param: any): Promise<ApiResult<T>> {
    const apiCall = () => this.sauce.get<T>(`${endpoint}/${param}`)
    const response = await apiCall()
    return this._getApiResult(response)
  }
  public async entityGetAll<T>(endpoint: string, param: any): Promise<ApiResult<Array<T>>> {
    const apiCall = () => this.sauce.get<Array<T>>(`${endpoint}?${qs.stringify(param)}`)
    const response = await apiCall()
    console.log(response.data)
    return this._getApiResult(response)
  }
  public async entityCreate<T>(endpoint: string, param: T): Promise<ApiResult<T>> {
    const apiCall = () => this.sauce.post<T>(endpoint, param)
    const response = await apiCall()
    return this._getApiResult(response)
  }
  public async entityUpdate<T>(endpoint: string, param: T): Promise<ApiResult<T>> {
    const apiCall = () => this.sauce.put<T>(endpoint, param)
    const response = await apiCall()
    return this._getApiResult(response)
  }
  public async entityRemove<T>(endpoint: string, param: any): Promise<ApiResult<boolean>> {
    const apiCall = () => this.sauce.delete<boolean>(`${endpoint}/${param}`)
    const response = await apiCall()
    return this._getApiResult(response)
  }

  public async calculatePurchaseOrder(param: IPurchaseOrder): Promise<ApiResult<IPurchaseOrder>> {
    const apiCall = () => this.sauce.post('api/purchase-orders/calculate', param)
    const response = await apiCall()
    return this._getApiResult(response)
  }

  /// General format ApiResponse into ApiResult
  private _getApiResult<T>(response: ApiResponse<T>): ApiResult<T> {
    const { ok, data, status, problem, originalError } = response
    if (ok) {
      return { ok, data }
    }
    return {
      ok: false,
      code: status,
      msg: `Lỗi ${status} - ${problem}`,
      data: data ? data : undefined,
    }
  }

  /// Format API result in error cases
  private _getErrorResult<T, U = any>(response: ApiResponse<T, U>): ApiResult<T, U> {
    const { data, status, problem, originalError } = response
    return {
      ok: false,
      code: status,
      msg: `${problem} - ${originalError}`,
      data: data as U
    }
  }

  private _getUnknownErrorResponse<T>(response: ApiResponse<T>, error: any) {
    const { problem, originalError } = response
    return { ok: false, code: -1, msg: `${problem} - ${originalError}`, data: error }
  }
}

export function createEntityApi<T>(api: JHipsterApi, path: string) {
  return {
    get: (id: any) => api.entityGet<T>(path, id),
    getAll: (param: any) => api.entityGetAll<T>(path, param),
    create: (param: T) => api.entityCreate<T>(path, param),
    update: (param: T) => api.entityUpdate<T>(path, param),
    remove: (id: any) => api.entityRemove<T>(path, id),
    put: (param: T) => {
      if ((param as any).id) {
        return api.entityUpdate<T>(path, param)
      } else {
        return api.entityCreate<T>(path, param)
      }
    }
  }
}