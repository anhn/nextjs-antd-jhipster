import React from 'react'
import { Form, Input, Button } from 'antd'
import { globalApi } from 'services/api'
import { FormProps } from 'antd/lib/form'

interface EntityFormProps extends FormProps {
  entity?: any
  onSubmit?: any
  onCancel?: any
}

export const ProductFormView = (props: EntityFormProps) => {
  const { entity, onSubmit, onCancel, ...restProps } = props
  return <Form onFinish={props.onSubmit} {...restProps}>
    <Form.Item label="ID" name="id" >
      <Input disabled />
    </Form.Item>
    <Form.Item label="NAME" name="name">
      <Input />
    </Form.Item>
    <Form.Item label="DESCRIPTION" name="description">
      <Input />
    </Form.Item>
    <Form.Item label="PRICE" name="price">
      <Input />
    </Form.Item>
    <Form.Item>
      {props.onSubmit ? <Button type="primary" htmlType="submit">Submit</Button> : null}
      {props.onCancel ? <Button onClick={props.onCancel}>Cancel</Button> : null}
    </Form.Item>
  </Form>
}

export const ProductForm = (props: EntityFormProps) => {
  const { entity } = props
  const [form] = Form.useForm()

  React.useEffect(() => {
    form.setFieldsValue(entity)
  }, [entity])

  return <ProductFormView
    labelCol={{ span: 8 }}
    wrapperCol={{ span: 12 }}
    form={form}
    onSubmit={props.onSubmit}
    onCancel={props.onCancel}
    {...props}
  />
}