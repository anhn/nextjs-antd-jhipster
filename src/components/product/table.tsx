import React from 'react'
import { Table } from 'antd'
import { TableProps } from 'antd/lib/table'
import { IProduct } from './entity'

const columns = [
  { title: 'ID', dataIndex: 'id', key: 'id' },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Price', dataIndex: 'price', key: 'price' }
]

export const ProductTableView = (props: TableProps<IProduct>) => {
  const { rowKey, columns: ignore, ...restProps } = props
  return <Table
    rowKey="id"
    columns={columns}
    {...restProps}
  />
}
