export class ProductEntity {
    id?: number
    name?: string
    sku?: string
    description?: string
    price?: number
    unitName?: string
    unitId?: number
}

export interface IProduct {
    id?: number;
    name?: string;
    sku?: string;
    description?: string;
    price?: number;
    unitName?: string;
    unitId?: number;
}
