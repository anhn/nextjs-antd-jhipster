import { Layout, Typography, Menu, Col, Row } from 'antd'
import { UserHeaderButton } from 'components/user-button'
import Link from 'next/link'

const { Header, Content } = Layout
const { Paragraph } = Typography

export const AppHeader = () => (
  <Header>
    <Row>
      <Col><UserHeaderButton /></Col>
      <Col>
        <Menu theme="dark" mode="horizontal">
          <Menu.Item key="order-form"><Link href="/edit/purchase-order"><a>Order Form</a></Link></Menu.Item>
          {/* <Menu.Item key="product"><Link href="/management/product"><a>Product</a></Link></Menu.Item>
          <Menu.Item key="order"><Link href="/management/purchase-order"><a>Order</a></Link></Menu.Item> */}
          <Menu.SubMenu key="sub" title="Management">
            <Menu.Item key="product"><Link href="/management/product"><a>Product</a></Link></Menu.Item>
            <Menu.Item key="order"><Link href="/management/purchase-order"><a>Order</a></Link></Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </Col>
    </Row>
  </Header>
)
