import { IOrderProductLine } from "components/order-product-line/entity"

export class PurchaseOrder {
    id?: number
    code?: string
    recipient?: string
    address?: string
    total?: number
}

export interface IPurchaseOrder {
    id?: number;
    code?: string;
    total?: number;
    created?: string;
    productLines?: IOrderProductLine[];
  }