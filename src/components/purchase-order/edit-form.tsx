import React from 'react'
import { Form, Input, Button, Select, message } from 'antd'
import { OrderProductLineCollection } from '../order-product-line/collection'
import { observer } from 'mobx-react-lite'
import { useRootStore } from 'model/root-store/root-store-context'
import { useProducts } from 'services/hooks/swrHooks'
import { FormProps } from 'antd/lib/form'
import { globalApi } from 'services/api'

interface EntityFormProps extends FormProps {
  entity?: any
  onSubmit?: any
  onCancel?: any
  onChange?: any
  productSelectOptions?: any[]
}

export const PurchaseOrderFormView = (props: EntityFormProps) => {
  const { entity, onSubmit, onCancel, ...restProps } = props
  const onOrderProductLinesChange = (data) => {
    console.log('onOrderProductLinesChange', data)
    if (props.onChange) {
      props.onChange(data)
    }
  }
  return <Form onFinish={props.onSubmit} {...restProps}>
    <Form.Item label="ID" name="id">
      <Input disabled />
    </Form.Item>
    <Form.Item label="CODE" name="code">
      <Input />
    </Form.Item>
    <Form.Item label="TOTAL" name="total">
      <Input />
    </Form.Item>
    <Form.Item label="PRODUCT LINES" name="productLines">
      <OrderProductLineCollection
        productSelectOptions={props.productSelectOptions}
        onChange={onOrderProductLinesChange}
      />
    </Form.Item>
    <Form.Item wrapperCol={{ offset: 8 }}>
      {props.onSubmit ? <Button type="primary" htmlType="submit">Submit</Button> : null}
      {props.onCancel ? <Button onClick={props.onCancel}>Cancel</Button> : null}
    </Form.Item>
  </Form>
}

export const PurchaseOrderForm = observer((props: EntityFormProps) => {
  const { hydrated } = useRootStore()
  const { entity } = props
  const [form] = Form.useForm()
  const formFields = ['id', 'code', 'total', 'productLines']

  React.useEffect(() => {
    form.setFieldsValue(entity)
  }, [entity])

  const allProducts = useProducts(hydrated)
  const productSelectOptions = React.useMemo(() => {
    return allProducts.map(p => <Select.Option key={p.id} value={p.id}>{p.name}</Select.Option>)
  }, [allProducts])

  const onFormValueChange = async () => {
    const value = form.getFieldsValue(formFields)
    console.log('onFormValueChange', value)

    const { ok, data, msg } = await globalApi.entities.purchaseOrder.calculate(value);
    if (ok) {
      form.setFieldsValue(data);
      // message.success('Thành công')
    } else {
      message.error(msg)
    }

    if (props.onChange) {
      props.onChange(value)
    }
  }

  return <PurchaseOrderFormView
    labelCol={{ span: 8 }}
    wrapperCol={{ span: 12 }}
    form={form}
    onSubmit={props.onSubmit}
    onCancel={props.onCancel}
    onChange={onFormValueChange}
    productSelectOptions={productSelectOptions}
    {...props}
  />
})
