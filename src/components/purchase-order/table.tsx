import React from 'react'
import { Table, Space } from 'antd'
import Link from 'next/link'
import { usePurchaseOrders } from 'services/hooks/swrHooks'
import { useRootStore } from 'model/root-store/root-store-context'
import { observer } from 'mobx-react-lite'
import { IPurchaseOrder } from './entity'
import { TableProps } from 'antd/lib/table'

const columns = [
  { title: 'ID', dataIndex: 'id', key: 'id' },
  { title: 'Code', dataIndex: 'code', key: 'code' },
  { title: 'Total', dataIndex: 'total', key: 'total' }
]

export const PurchaseOrderTableView = (props: TableProps<IPurchaseOrder>) => {
  const { rowKey, columns: ignore, ...restProps } = props
  return <Table
    rowKey="id"
    columns={columns}
    {...restProps}
  />
}

export const PurchaseOrderTable = observer(() => {
  const { hydrated } = useRootStore()
  const data = usePurchaseOrders(hydrated)
  return <PurchaseOrderTableView dataSource={data} />
})