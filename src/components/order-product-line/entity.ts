export interface IOrderProductLine {
    id?: number;
    quantity?: number;
    subTotal?: number;
    productName?: string;
    productId?: number;
    purchaseOrderCode?: string;
    purchaseOrderId?: number;
  }
  