import React from 'react'
import { Button, Space, Row } from 'antd'
import { OrderProductLineForm } from './edit-form'
import { OrderProductLineTableView } from './table'

type ManagementProps = {
  value?: any[]
  onChange?: any
  onCreateItem?: (item) => Promise<any>
  onUpdateItem?: (item) => Promise<any>
  onRemoveItem?: (item) => Promise<any>
  productSelectOptions?: any[]
}

export const OrderProductLineCollection = (props: ManagementProps) => {
  const [data, setData] = React.useState([])
  const [removeData, setRemoveData] = React.useState([])
  const [editting, setEditting] = React.useState(false)
  const [edittingItem, setEdittingItem] = React.useState({})
  const [selectedRows, setSelectedRows] = React.useState([])

  React.useEffect(() => {
    if (props.value) {
      onNotifiedDataChange(props.value)
    }
  }, [props.value])

  const upstreamNotifyDataChange = (_data, _removeData) => {
    console.log('upstreamNotifyDataChange', _data, _removeData)
    if (props.onChange) {
      props.onChange(_data.concat(_removeData))
    }
  }
  // In coming upstream data, to be merged in
  const onNotifiedDataChange = (value) => {
    const newData = value.filter(item => !item.markDelete)
    const newRemoveData = value.filter(item => item.markDelete)
    console.log('incoming', newData, newRemoveData)
    setData(newData)
    setRemoveData(newRemoveData)
  }

  // Finish editting a row
  const onRowEditSubmit = item => {
    console.log('Submit item', item)
    if (!item.id) {
      const newData = data.concat(item)
      setData(newData)
      upstreamNotifyDataChange(newData, removeData)
      setEditting(false)
      ///// SUBMIT creating item here
      if (props.onCreateItem) {
        props.onCreateItem(item)
      }
      return
    }
    const index = data.findIndex(itm => itm.id === item.id)
    if (index >= 0) {
      const newData = Array.from(data)
      newData.splice(index, 1, item)
      setData(newData)
      upstreamNotifyDataChange(newData, removeData)
    }
    setEditting(false)
    ///// SUBMIT editing item here
    if (props.onUpdateItem) {
      props.onUpdateItem(item)
    }
  }

  const onCancelEditting = () => {
    setEditting(false)
  }

  const onStartCreatingRow = () => {
    setEditting(true)
    setEdittingItem({})
  }

  const onStartEdittingRow = () => {
    if (selectedRows.length == 0) return
    const row = selectedRows[0]

    setEditting(true)
    setEdittingItem(row)
  }

  const onStartDeletingRow = () => {
    if (selectedRows.length == 0) return
    const row = selectedRows[0]

    const index = data.findIndex(item => item.id === row.id)
    const newData = data.slice()
    const removed = newData.splice(index, 1)
    setData(newData)

    removed[0].markDelete = true
    const newRemoveData = removeData.concat(removed[0])
    setRemoveData(newRemoveData)
    upstreamNotifyDataChange(newData, newRemoveData)

    ///// SUBMIT editing item here
    if (props.onRemoveItem) {
      props.onRemoveItem(newData[index])
    }
  }

  const rowSelection = {
    type: 'radio' as any,
    onChange: (selectedKeys, selectedRows) => {
      setSelectedRows(selectedRows)
    },
    getCheckboxProps: record => ({
      name: record.name,
    }),
  };

  return <Space direction="vertical" style={{ width: '100%' }}>
    <Row><Space>
      <Button onClick={onStartCreatingRow}>Create</Button>
      <Button disabled={selectedRows.length == 0} onClick={onStartEdittingRow}>Edit</Button>
      <Button disabled={selectedRows.length == 0} onClick={onStartDeletingRow}>Delete</Button>
    </Space></Row>
    {editting ? <OrderProductLineForm
      entity={edittingItem}
      productSelectOptions={props.productSelectOptions}
      onSubmit={onRowEditSubmit}
      onCancel={onCancelEditting}
    /> :
      <OrderProductLineTableView
        dataSource={data}
        rowSelection={rowSelection}
      />
    }
  </Space>
}
