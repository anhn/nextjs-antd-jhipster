import React from 'react'
import { Table } from 'antd'
import { TableProps } from 'antd/lib/table'
import { IOrderProductLine } from './entity'

let columns = [
  { title: 'ID', dataIndex: 'id', key: 'id' },
  { title: 'Product', dataIndex: 'productName', key: 'productName' },
  { title: 'Quantity', dataIndex: 'quantity', key: 'quantity' },
  { title: 'Sub total', dataIndex: 'subTotal', key: 'subTotal' }
]

export const OrderProductLineTableView = (props: TableProps<IOrderProductLine>) => {
  const { rowKey, columns: ignore, ...restProps } = props
  return <Table
    rowKey="id"
    columns={columns}
    {...restProps}
  />
}
