import React from 'react'
import { Form, Input, Button, Select } from 'antd'
import { FormProps } from 'antd/lib/form'

interface EntityFormProps extends FormProps {
  entity?: any
  onSubmit?: any
  onCancel?: any
  productSelectOptions?: any[]
}

export const OrderProductLineFormView = (props: EntityFormProps) => {
  const { entity, onSubmit, onCancel, productSelectOptions, ...restProps } = props
  return <Form onFinish={onSubmit} {...restProps}>
    <Form.Item label="ID" name="id">
      <Input disabled />
    </Form.Item>
    <Form.Item label="QUANTITY" name="quantity">
      <Input />
    </Form.Item>
    <Form.Item label="PRODUCT" name="productId">
      <Select style={{ minWidth: 150 }}>
        {productSelectOptions}
      </Select>
    </Form.Item>
    <Form.Item name="productName" hidden/>
    <Form.Item>
      {onSubmit ? <Button type="primary" htmlType="submit">Submit</Button> : null}
      {onCancel ? <Button onClick={onCancel}>Cancel</Button> : null}
    </Form.Item>
  </Form>
}

export const OrderProductLineForm = (props: EntityFormProps) => {
  const { entity } = props
  const isEditting = typeof entity !== 'undefined'
  const orderProductLine = entity || {}
  const [form] = Form.useForm()

  React.useEffect(() => {
    form.setFieldsValue(orderProductLine)
  }, [entity])

  return <OrderProductLineFormView
    // labelCol={{ span: 8 }}
    // wrapperCol={{ span: 12 }}
    form={form}
    onSubmit={props.onSubmit}
    onCancel={props.onCancel}
    {...props}
  />
}