import React from 'react'
import { observer } from 'mobx-react-lite'
import { Input, Typography, Form, Button, message, Tabs, Row, Col } from 'antd'
import { UserOutlined, LockOutlined, PhoneOutlined, NumberOutlined } from '@ant-design/icons'
import { useRootStore } from 'model/root-store/root-store-context'

const { Text, Title } = Typography
type Props = {
  onAuthenticatedChange?: (value: any) => void
}

export type LoginType = 'account' | 'mobile'

export const LoginBox = observer((props: Props) => {
  const [loginType, setLoginType] = React.useState('account')
  const [login, setLogin] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [code, setCode] = React.useState('')

  const rootStore = useRootStore()
  const auth = rootStore.auth

  const [submitting, setSubmitting] = React.useState(false)

  React.useEffect(() => {
    if (props.onAuthenticatedChange) {
      if (auth.isAuthenticated) {
        props.onAuthenticatedChange(auth.isAuthenticated)
      }
    }
  }, [auth.isAuthenticated])

  const doLogin = async (e: any) => {
    if (e && e.preventDefault) {
      e.preventDefault()
    }
    let response: any = {}
    setSubmitting(true)
    try {
      switch (loginType) {
        case 'account':
          response = await auth.loginPassword(login, password)
          break
      }
    } finally {
      setSubmitting(false)
    }
    if (!response.ok) {
      message.error(`Đăng nhập chưa thành công. Lỗi ${response.code}`, 2)
    }
  }

  return <>
    <Form onFinish={doLogin} >
      <Tabs activeKey={loginType} onChange={setLoginType}>
        <Tabs.TabPane key="account" tab="Tài khoản">
          <Form.Item>
            <Input
              placeholder="Đăng nhập"
              onChange={(e) => setLogin(e.target.value)}
              value={login}
              prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
          </Form.Item>
          <Form.Item>
            <Input
              placeholder="Mật khẩu"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
          </Form.Item>
        </Tabs.TabPane>
        <Tabs.TabPane key="mobile" tab="Số điện thoại">
          <Form.Item>
            <Input
              placeholder="Số điện thoại"
              prefix={<PhoneOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
          </Form.Item>
          <Form.Item>
            <Row gutter={8}>
              <Col span={16}><Input
                onChange={(e) => setCode(e.target.value)}
                value={code}
                placeholder="Mã xác thực"
                prefix={<NumberOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                style={{ display: 'block-inline' }}
              /></Col>
              <Col span={8}>
                <Button style={{ width: '100%' }}>Gửi mã xác thực</Button>
              </Col>
            </Row>
          </Form.Item>
        </Tabs.TabPane>
      </Tabs>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          block={true}
          loading={submitting}
        >
          Đăng nhập
        </Button>
      </Form.Item>
    </Form>
    {/* <Button type="link" icon={<FacebookOutlined />} onClick={signInWithFacebook}>Đăng nhập bằng Facebook</Button>
    <Button type="link" icon={<GithubOutlined />} onClick={signInWithGithub}>Đăng nhập bằng Github</Button> */}
  </>
})
