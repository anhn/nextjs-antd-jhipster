import React from 'react'
import { LoginOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons'
import { Menu, Button, Dropdown, Tooltip, Modal, Typography } from 'antd'
import { observer } from 'mobx-react-lite'
import { useRootStore } from 'model/root-store/root-store-context'
import { LoginBox } from './login-box'

const loginStyle = {
  // marginLeft: 15,
  color: '#fe980f',
  width: 40,
  height: 40,
  border: '1px solid #fe980f',
  borderRadius: 40,
}
const headerIconButtonStyle = {
  width: 40,
  height: 40,
  border: '1px solid #fe980f',
  color: '#fe980f',
  borderRadius: 40,
}

const { Text, Title } = Typography

export const UserHeaderButton = observer(props => {
  const { auth, app } = useRootStore()
  const [visible, setVisible] = React.useState(false)
  const showLogoutConfirm = () => {
    Modal.confirm({
      title: 'Bạn có chắc chắn ?',
      okText: 'OK',
      cancelText: 'Thôi',
      onOk: () => {
        auth.logout()
      },
    })
  }

  const handleMenuAction = ({ key, keyPath, item }) => {
    switch (key) {
      case 'logout':
        showLogoutConfirm()
        break
      default:
        break
    }
  }

  const showLogin = () => app.showLoginModal(true)
  const hideLogin = () => app.showLoginModal(false)

  const menu = (
    <Menu onClick={handleMenuAction}>
      <Menu.Item key="">
        <Typography.Text strong style={{ maxWidth: 96 }}>
          {auth.user ? `User#${auth.user.id}` : 'TBD'}
        </Typography.Text>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="logout">
        <LogoutOutlined />
        <Text>Đăng xuất</Text>
      </Menu.Item>
    </Menu>
  )
  if (auth.isAuthenticated) {
    if (app.loginModalVisible) {
      hideLogin()
    }
    return (
      <Dropdown overlay={menu} onVisibleChange={setVisible} visible={visible} trigger={['click', 'hover']}>
        <Button
          icon={<UserOutlined />}
          style={headerIconButtonStyle}
        />
      </Dropdown>
    )
  } else {
    return (
      <>
        <Tooltip title="Đăng nhập">
          <Button icon={<LoginOutlined />} style={loginStyle} onClick={showLogin} />
        </Tooltip>
        <Modal
          visible={app.loginModalVisible}
          footer={null}
          closable={false}
          style={{ borderRadius: 16 }}
          onCancel={hideLogin}
        >
          <LoginBox />
        </Modal>
      </>
    )
  }
})
