const withLessAntd = require('./antd/nextLessAntd')

module.exports = (phase, { defaultConfig }) => {
  /* eslint-disable */
  const lessToJS = require('less-vars-to-js')
  const fs = require('fs')
  const path = require('path')  // Where your antd-custom.less file lives
  const themeVariables = lessToJS(
    fs.readFileSync(path.resolve(__dirname, './antd/antd-custom.less'), 'utf8')
  )
  return withLessAntd({
    cssModules: true,
    lessLoaderOptions: {
      javascriptEnabled: true,
      modifyVars: themeVariables // make custom theme effective
    },
    webpack: (config, options) => {
      if (config.node) {
        config.node.fs = 'empty'
      } else {
        config.node = { fs: 'empty' }
      }
      config.module.rules.push({
        test: /_tests_/,
        loader: 'ignore-loader'
      })
      return config
    }
  })
};